#include "connection.hpp"
#include "httpd.hpp"
using namespace tcplisten;

#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
using namespace std;



void serverLoop() {
    // Create listener
    Listener listener(HTTPD::config.port);
    // Create server callback
    auto server = [&] (int raw_fd, ...) {
        listener.ignore(raw_fd);
        thread([&, raw_fd] () {
            {
                Fdstream<false> fd(raw_fd);
                HTTPD::HTTPD(fd).run();
            }
            listener.disconnect(raw_fd);
        }).detach();
    };
    // Run listener in a loop
    while (true) {
        listener.callback(server);
    }
}

int main(int argc, char **argv) {
    if (argc == 1) {
        serverLoop();
    } else {
        if (strcmp(argv[1], "config") == 0) {
            std::cout << "export PORT=" << HTTPD::config.port << "\n"
                         "export SUUSER=" << HTTPD::config.su_user << endl;
        }
    }
}
