#include <string>
#include <filesystem>


namespace HTTPD {

struct Config {
    std::filesystem::path files;
    std::filesystem::path error_404, error_403;
    int port;
    std::string su_user;
} extern config;

}
