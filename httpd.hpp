#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <fmt/format.h>
#include <unistd.h>
#include "magic.h"
#include "config.hpp"



namespace HTTPD {
std::string get_content_type(const std::filesystem::path& path) {
    // Check if file ending is known for being mismatched by libmagic
    auto ext = path.extension().string();
    if (ext == ".css") {
        return "text/css";
    } else {
        // Else use libmagic
        auto cookie = magic_open(MAGIC_MIME | MAGIC_SYMLINK);
        magic_load(cookie, nullptr);
        std::string fres = magic_file(cookie, path.c_str());
        magic_close(cookie);
        return fres;
    }
}

void urlDecode(std::string::const_iterator& in, std::ostringstream& out) { // Decodes at iterator
    if (*in == '%') {
        in += 2;
        char code = std::stoi(std::string(in-1, in+1), 0, 16);
        out << code;
    } else if (*in == '+') {
        out << ' ';
    } else {
        out << *in;
    }
}

std::string urlDecode(const std::string& in) { // Decodes full URL
    std::ostringstream out;
    // Decode loop
    for (auto it = in.begin(); it != in.end(); it++) {
        // Stop at '?' character
        if (*it == '?') {
            break;
        }
        // Else, just continue decoding
        urlDecode(it, out);
    }
    // Return result
    return out.str();
}

template<typename streamT>
class HTTPD {
    using Iobak = std::array<int, 2>;

    std::string status = "HTTP/1.0 200 OK",
                content_type = "text/html",
                url;
    std::filesystem::path file;


public:
    streamT &stream;

    HTTPD(streamT& stream)
        : stream(stream) {}

    void run() {
        try {
            // Read the HTTP-request
            readrequest();
            // Some URL checks and modifications
            if (urlcheck()) {
                // Write finished file
                sendfile();
            }
        }  catch (std::exception& e) {
            stream << fmt::format("HTTP/1.0 400 Bad Request\n"
                                  "Content-Type: text/html\n"
                                  "\n"
                                  "<b>Error parsing request:</b> <i>{}</i>\n", e.what());
        }
    }

    void readrequest() {
        // Read header until url
        stream >> url;
        stream >> url;
        file = std::filesystem::weakly_canonical(config.files / urlDecode(url.substr(1, url.size() - 1)));
    }

    bool urlcheck() {
        // Block tries to read documents outside the document root
        if (file.string().find(config.files) != 0) {
            url = "/";
            file = config.files;
        }
        // Avoid a bug
        if (std::filesystem::is_directory(file) and url.back() != '/') {
            status = "HTTP/1.0 302 Moved permanently\n"
                     "Location: " + url + "/";
            httpheaders();
            return false;
        }
        // Find index.html
        if (std::filesystem::is_regular_file(file/"index.html")) {
            url += "index.html";
            file /= "index.html";
        }
        // Respond 404 if the file doesn't exist
        if (not std::filesystem::exists(file)) {
            status = "HTTP/1.0 404 Not Found";
            file = config.files / config.error_404;
        }
        // Respond 403 if file isn't readable
        else if (access(file.c_str(), R_OK) != 0) {
            status = "HTTP/1.0 403 Forbidden";
            file = config.files / config.error_403;
        }
        return true;
    }

    void sendfile() {
        if (std::filesystem::is_regular_file(file)) {
            content_type = get_content_type(file);
            httpheaders();
            std::ifstream f(file);
            stream << f.rdbuf();
        } else {
            httpheaders();
            filelist();
        }
    }

    void httpheaders() {
        stream << fmt::format("{}\n"
                              "Content-Type: {}\n"
                               "\n", status, content_type);
        // in_flush if possible
#       if __cplusplus == 202002
        if constexpr(requires(const streamT& t) {
            t.in_flush();
        }) {
            stream.in_flush();
        }
#       endif
    }

    void filelist() {
        stream << fmt::format("<html>"
                              "    <head>"
                              "        <title>Index of: {0}</title>"
                              "    </head>"
                              "    <body>"
                              "        <p>{0}</p>"
                              "        <h2>Directory list:</h2><br />"
                              "        <a href=\"../\">..</a><br />", url);
        for (const auto& i : std::filesystem::directory_iterator(file)) {
            auto filename = i.path().filename().string();
            stream << fmt::format("        <a href=\"./{0}\">{0}</a><br />", filename);
        }
        stream << "    </body>"
                  "</html>";
    }
};

}
