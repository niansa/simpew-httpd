#include "config.hpp"

namespace HTTPD {
Config config = {
    // Here is the webroot (homefolder for the website)
    .files = "/srv/www",

    // Error code pages, they need to be HTML documents. Relative to webroot if not prefixed with /
    .error_404 = "404.html",
    .error_403 = "403.html",

    // This port will used for listening
    .port = 8888
};
}
